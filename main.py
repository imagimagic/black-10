from __future__ import annotations

import time
from dataclasses import dataclass
from pathlib import Path
from typing import Callable

import numpy as np
import easygui
import cv2 as cv
import pymsgbox


def select_image(prompt: str) -> np.ndarray:
    if (path_str := easygui.fileopenbox(title=prompt)) is None:
        exit(0)

    file_path = Path(path_str)

    if (image := cv.imread(str(file_path))) is None:
        pymsgbox.alert(title='Error', text=f'Could not open file:\n{file_path}', icon=pymsgbox.WARNING)
        exit(1)

    return image


@dataclass
class Model:
    on_values_changed: Callable[[Model], None]

    max_matches: int = 10

    def set_max_matches(self, value: int) -> None:
        self.max_matches = max(1, value)
        self.on_values_changed(self)


def main() -> None:
    target_image = select_image('Select target image')
    target_gray = cv.cvtColor(target_image, cv.COLOR_BGR2GRAY)
    print(1)
    time.sleep(1)
    scene_image = select_image('Select scene image')
    scene_gray = cv.cvtColor(scene_image, cv.COLOR_BGR2GRAY)
    print(2)

    # Initiate ORB detector
    orb = cv.ORB_create()

    # find the key points and descriptors with ORB
    kp_target, des_target = orb.detectAndCompute(target_gray, None)
    kp_scene, des_scene = orb.detectAndCompute(scene_gray, None)

    bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)

    matches = bf.match(des_target, des_scene)
    # Sort them in the order of their distance.
    matches = sorted(matches, key=lambda x: x.distance)

    window_name = 'Matches'
    cv.namedWindow(window_name, cv.WINDOW_GUI_EXPANDED)

    def redraw(model_: Model) -> None:
        result_image = cv.drawMatches(
            target_image, kp_target, scene_image, kp_scene, matches[:model_.max_matches], None,
            flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS
        )
        cv.imshow(window_name, result_image)

    model = Model(redraw)

    cv.createTrackbar('max matches', window_name, model.max_matches, 150, model.set_max_matches)

    cv.waitKey(0)
    cv.destroyAllWindows()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
